<h4 id="trains-unifiednlp">Trains UnifiedNLP</h4>
<p>This is a UnifiedNLP provider that detects if you are connected to a
trains wifi and, if so, provides the location data provided by the train
to your android phone.</p>
<h5 id="currently-supported-trains">Currently supported trains</h5>
<ul>
<li>🇨🇿 České Dráhy (InterJet, Pendolino, some IC)</li>
<li>🇩🇪 Deutsche Bahn (ICE, some IC)</li>
<li>🇭🇺 Magyar Államvasutak</li>
</ul>
