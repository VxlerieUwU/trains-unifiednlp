/*
 * Trains UnifiedNLP
 * Copyright (C) 2022 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.SupplicantState;
import android.util.Log;
import org.microg.nlp.api.LocationBackendService;

public class TrainsService extends LocationBackendService {
	private static final String TAG = TrainsService.class.getName();

	private Provider currentProvider = null;
	private BroadcastReceiver broadcastReceiver = null;

	private Provider createProvider(String ssid) {
		switch(ssid) {
			case "CDWiFi":
				return new CDProvider(l -> report(l));
			case "MAVSTART-WIFI":
				return new MAVProvider(l -> report(l));
			case "WIFIonICE":
			case "WIFI@DB":
				return new DBProvider(l -> report(l));
			default:
//				return new TestProvider(l -> report(l));
				return null;
		}
	}

	private void stopProvider() {
		if (currentProvider != null) {
			currentProvider.stop();
			currentProvider = null;
		}
		Utils.clearCache();
	}

	private void startProvider(String ssid) {
		stopProvider();
		currentProvider = createProvider(ssid);
		if (currentProvider != null) {
			currentProvider.start();
		}
	}

	private String getSsid() {
		WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

		if (wifiManager == null) {
			return null;
		}

		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		if (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
			// getSSID returns the ssid surrounded by double quotes,
			// so we have to strip them
			String ssid = wifiInfo.getSSID();
			return ssid.substring(1, ssid.length() - 1);
		} else {
			return null;
		}
	}

	@Override
	protected Location update() {
		Log.d(Provider.class.getName(), "Got request to update");
		if (currentProvider != null) {
			return currentProvider.update();
		}
		Log.d(TAG, "No provider");
		return null;
	}

	@Override
	protected void onOpen() {
		super.onOpen();

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

		broadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String ssid = getSsid();
				if (ssid == null) {
					// disconnected
					Log.d(TAG, "Disconnected from wifi");
					stopProvider();
				} else {
					Log.d(TAG, "Connected to wifi SSID " + ssid);
					startProvider(ssid);
				}
			}
		};
		registerReceiver(broadcastReceiver, intentFilter);

	}

	@Override
	protected void onClose() {
		stopProvider();
		if (broadcastReceiver != null) {
			unregisterReceiver(broadcastReceiver);
		}
	}
}
