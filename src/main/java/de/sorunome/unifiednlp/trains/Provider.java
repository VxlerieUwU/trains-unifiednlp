/*
 * Trains UnifiedNLP
 * Copyright (C) 2022 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains;

import android.location.Location;
import android.util.Log;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Consumer;

public abstract class Provider {
	final private Consumer<Location> reportFn;
	protected Location lastLocation;
	protected Location lastValidLocation;
	protected Date lastTime;
	public Provider(Consumer<Location> report){
		this.reportFn = report;
	}

	// Report a location. Reporting null indicates that the trains GPS is invalid currently.
	protected void report(Location l) {
		lastLocation = l;
		if (l != null) {
			lastValidLocation = l;
		}
		lastTime = Calendar.getInstance().getTime();
		if (l != null) {
			reportFn.accept(l);
		}
	}

	public abstract void start();

	public abstract void stop();

	protected Location update() {
		if (lastLocation == null || lastTime == null) {
			Log.d(Provider.class.getName(), "No last location, returning...");
			return null;
		}
		// only re-report the location if it isn't older than 30 seconds
		if (Calendar.getInstance().getTime().getTime() - lastTime.getTime() < 30000) {
			lastLocation.setTime(Calendar.getInstance().getTime().getTime());
			Log.d(Provider.class.getName(), "Reporting " + lastLocation);
			return lastLocation;
		}
		Log.d(Provider.class.getName(), "Too much time passed, returning...");
		return null;
	}
}
