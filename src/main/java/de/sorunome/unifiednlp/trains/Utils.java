/*
 * Trains UnifiedNLP
 * Copyright (C) 2022 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains;

import android.location.Location;
import android.util.Log;
import com.barbeaudev.geotools.referencing.operation.transform.EarthGravitationalModel;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class Utils {
	private static final String TAG = Utils.class.getName();

	private static EarthGravitationalModel gh = null;

	public static String getUrl(String urlString) throws IOException {
		HttpURLConnection urlConnection = null;
		URL url = new URL(urlString);
		urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("GET");
		urlConnection.setReadTimeout(10000); // milliseconds
		urlConnection.setConnectTimeout(15000); // milliseconds
		urlConnection.setDoOutput(true);
		urlConnection.connect();

		BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
		StringBuilder sb = new StringBuilder();

		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line + "\n");
		}
		br.close();

		return sb.toString();
	}

	public static void setAltitudeMeters(Location location, double altitude) {
		if (gh == null) {
			gh = new EarthGravitationalModel();
			try {
				gh.load("/egm180.nor");
			} catch (IOException e) {
				Log.d(TAG, "Failed to load gravitational model " + e);
				return;
			}
		}
		double approxOffset = 0;
		for (int i = 0; i < 2; i++) {
			approxOffset = gh.heightOffset(location.getLongitude(), location.getLatitude(), altitude + approxOffset);
		}
		location.setAltitude(altitude + approxOffset);
	}

	public static void clearCache() {
		if (gh != null) {
			gh = null;
		}
	}
}
